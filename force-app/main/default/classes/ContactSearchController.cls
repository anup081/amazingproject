
//Class to contact search component
public with sharing class ContactSearchController {
     
    
    //fetch all contact records from the org
    @AuraEnabled  
   public static List<Contact> loadData(){  
     List<Contact> conList = [select Id,Name, FirstName, LastName, Email from Contact order by Name asc];  
     return conList;  
   }  
}
